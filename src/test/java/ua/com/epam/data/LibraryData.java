package ua.com.epam.data;

import jdk.nashorn.internal.runtime.options.Options;
import ua.com.epam.mode.author.Author;
import ua.com.epam.mode.author.Birth;
import ua.com.epam.mode.author.Name;
import ua.com.epam.mode.book.Additional;
import ua.com.epam.mode.book.Book;
import ua.com.epam.mode.book.Size;
import ua.com.epam.mode.geren.Genre;

import java.time.LocalDate;
import java.util.Random;

public class LibraryData {

    public Long getRandomId() {
        Random rd = new Random();
        return (long) (rd.nextInt((9999999 - 1) + 1) + 1);
    }

    public Author getAuthorWithBaseData() {
        Name name = new Name();
        name.setFirst("a");
        name.setSecond("a");

        Birth birth = new Birth();
        birth.setDate(LocalDate.ofEpochDay(1111 - 11 - 11));
        birth.setCountry("a");
        birth.setCity("a");

        Author author = new Author();
        author.setAuthorId(getRandomId());
        author.setAuthorName(name);
        author.setNationality("a");
        author.setBirth(birth);
        author.setAuthorDescription("a");
        return author;
    }

    public Book getBookWithBaseData() {
        Size size = new Size();
        size.setHeight(1.1);
        size.setWidth(1.1);
        size.setLength(1.1);

        Additional additional = new Additional();
        additional.setPageCount(1L);
        additional.setSize(size);

        Book book = new Book();
        book.setBookId(getRandomId());
        book.setBookName("a");
        book.setBookLanguage("a");
        book.setBookDescription("a");
        book.setAdditional(additional);
        return book;
    }

    public Genre getGenreWithBaseData() {

        Genre genre = new Genre();
        genre.setGenreId(getRandomId());
        genre.setGenreName("a");
        genre.setGenreDescription("a");
        return genre;
    }
}
