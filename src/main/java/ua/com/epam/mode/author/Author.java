package ua.com.epam.mode.author;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Author {

    private Long authorId;
    private Name authorName;
    private String nationality;
    private Birth birth;
    private String authorDescription;
}
