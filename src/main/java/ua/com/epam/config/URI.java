package ua.com.epam.config;

public interface URI {
    DataProp dp = new DataProp();
    String BASE_URI = dp.endpointUrl();

    //Author
    String GET_AUTHOR_BY_ID = "/api/library/author/%s";
    String GET_AUTHOR_BY_BOOK = "/api/library/book/%s/author%s";
    String GET_ALL_AUTHORS = "/api/library/authors%s";
    String SEARCH_AUTHORS_BY_NAME_OR_SURNAME = "/api/library/authors/search?query=%s";
    String GET_ALL_AUTHORS_BY_GENRE = "/api/library/genre/%s/authors%s";
    String POST_AUTHOR = "/api/library/author";
    String PUT_AUTHOR = "/api/library/author/%s";
    String DELETE_AUTHOR_BY_ID = "/api/library/author/%s";

    //Book
    String GET_ALL_BOOKS_BY_AUTHOR_ID= "/api/library/author/%s/books%s";
    String GET_ALL_BOOKS_BY_AUTHOR_AND_GENRE_ID= "/api/library/author/%s/genre/%s/books";
    String UPDATE_BOOKS = "/api/library/book";
    String CREATE_BOOKS = "/api/library/book/%s/%s";
    String GET_BOOK_BY_ID = "/api/library/book/%s";
    String DELETE_BOOK_BY_ID = "/api/library/book/%s";
    String GET_ALL_BOOKS = "/api/library/books%s";
    String SEARCH_BOOK_BY_NAME = "/api/library/books/search?query=%s";
    String SEARCH_BOOK_BY_GENRE_ID = "/api/library/genre/%s/books%s";

    //Genre
    String GET_ALL_GENRE_BY_AUTHOR_ID = "/api/library/author/%s/genres%s";
    String GET_GENRE_BY_AUTHOR_ID = "/api/library/book/%s/genre";
    String CREATE_GENRE = "/api/library/genre";
    String UPDATE_GENRE = "/api/library/genre";
    String GET_GENRE_BY_ID = "/api/library/genre/%s";
    String DELETE_GENRE_BY_ID = "/api/library/genre/%s";
    String GET_ALL_GENRES = "/api/library/genres%s";
    String SEARCH_GENRE_NAME = "/api/library/genres/search?query=%s";
}
