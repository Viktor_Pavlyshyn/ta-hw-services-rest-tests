package ua.com.epam.client;

import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;

import static io.restassured.RestAssured.given;
import static ua.com.epam.config.PropConstant.CONTENT_TYPE;
import static ua.com.epam.config.URI.BASE_URI;

@Log4j2
public class REATClient {

    public Response get(String uri) {

        log.info("Perform GET request to: '{}'.", uri);
        Response response = given().contentType(CONTENT_TYPE)
                .get(String.format(BASE_URI, uri));

        return response;
    }

    public <T> Response post(String uri, T body) {

        log.info("Perform POST request to: '{}' with body: '{}'.", uri, body);

        Response response = given().contentType(CONTENT_TYPE).body(body)
                .post(String.format(BASE_URI, uri));
        return response;
    }

    public <T> Response put(String uri, T body) {

        log.info("Perform PUT request to: '{}' with body: '{}'.", uri, body);

        Response response = given().contentType(CONTENT_TYPE).body(body)
                .put(String.format(BASE_URI, uri));
        return response;
    }

    public Response delete(String uri) {

        log.info("Perform DELETE request to: '{}'.", uri);

        Response response = given().contentType(CONTENT_TYPE)
                .delete(String.format(BASE_URI, uri));

        return response;
    }
}
