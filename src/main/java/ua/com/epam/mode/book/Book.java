package ua.com.epam.mode.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    private Long bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;
    private Additional additional;
}
