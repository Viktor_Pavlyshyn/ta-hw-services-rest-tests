package ua.com.epam;

import io.restassured.response.Response;
import org.testng.annotations.Test;
import ua.com.epam.mode.Fault;
import ua.com.epam.mode.author.Author;

import static ua.com.epam.config.AssertMessage.ERROR_AUTHOR_MSG;

public class DeleteAuthorTest extends BaseTest{
    @Test
    public void deleteAuthor() {
        Author author = libraryData.getAuthorWithBaseData();

        Response authorResp =  authorService.createAuthor(author);
        validator.checkStatusCode(201, authorResp.getStatusCode());

        Response response = authorService.deleteAuthor(String.valueOf(author.getAuthorId()));
        validator.checkStatusCode(204, response.getStatusCode());

        Response responseEnd = authorService.deleteAuthor(String.valueOf(author.getAuthorId()));
        validator.checkStatusCode(404, responseEnd.getStatusCode());
    }

    @Test
    public void deleteNotExistAuthor() {
        Author author = libraryData.getAuthorWithBaseData();

        Response authorResp =  authorService.createAuthor(author);
        validator.checkStatusCode(201, authorResp.getStatusCode());

        Response responseDelete = authorService.deleteAuthor(String.valueOf(author.getAuthorId()));
        validator.checkStatusCode(204, responseDelete.getStatusCode());

        Response response = authorService.deleteAuthor(String.valueOf(author.getAuthorId()));
        validator.checkStatusCode(404, response.getStatusCode());

        Fault fault = response.jsonPath().getObject("$", Fault.class);
        String errorMsg = fault.getErrorMessage();

        validator.checkResponseObject(errorMsg, String.format(ERROR_AUTHOR_MSG,author.getAuthorId()));
    }
}
