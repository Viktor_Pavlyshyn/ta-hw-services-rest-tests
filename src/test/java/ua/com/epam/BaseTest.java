package ua.com.epam;

import org.testng.annotations.BeforeMethod;
import ua.com.epam.data.LibraryData;
import ua.com.epam.client.REATClient;
import ua.com.epam.validator.Validator;
import ua.com.epam.service.AuthorService;
import ua.com.epam.service.BookService;
import ua.com.epam.service.GenreService;

public class BaseTest {
    REATClient client = new REATClient();
    AuthorService authorService = new AuthorService(client);
    GenreService genreService = new GenreService(client);
    BookService bookService = new BookService(client);
    Validator validator;
    LibraryData libraryData;

    @BeforeMethod
    public void reinitialize() {
        this.validator = new Validator();
        this.libraryData = new LibraryData();
    }
}
