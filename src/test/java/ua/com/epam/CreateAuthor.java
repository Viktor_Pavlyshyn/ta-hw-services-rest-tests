package ua.com.epam;

import io.restassured.response.Response;
import org.testng.annotations.Test;
import ua.com.epam.mode.Fault;
import ua.com.epam.mode.author.Author;

import static ua.com.epam.config.AssertMessage.ERROR_AUTHOR_MSG_CREATE;

public class CreateAuthor extends BaseTest {
    @Test
    public void createAuthor() {
        Author author = libraryData.getAuthorWithBaseData();

        Response response = authorService.createAuthor(author);
        validator.checkStatusCode(201, response.getStatusCode());

        Author authorResp = response.jsonPath().getObject("$", Author.class);
        validator.checkResponseObject(authorResp, author);

        Response deleteAuthor = authorService.deleteAuthor(String.valueOf(author.getAuthorId()));
        validator.checkStatusCode(204, deleteAuthor.getStatusCode());
    }

    @Test
    public void createExistAuthor() {
        Author author = libraryData.getAuthorWithBaseData();

        Response createAuthor = authorService.createAuthor(author);
        validator.checkStatusCode(201, createAuthor.getStatusCode());

        Response response = authorService.createAuthor(author);
        validator.checkStatusCode(409, response.getStatusCode());

        Fault fault = response.jsonPath().getObject("$", Fault.class);
        String errorMsg = fault.getErrorMessage();

        validator.checkResponseObject(errorMsg, ERROR_AUTHOR_MSG_CREATE);

        Response deleteAuthor = authorService.deleteAuthor(String.valueOf(author.getAuthorId()));
        validator.checkStatusCode(204, deleteAuthor.getStatusCode());
    }
}
