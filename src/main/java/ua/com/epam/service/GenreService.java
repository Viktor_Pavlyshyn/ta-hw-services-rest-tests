package ua.com.epam.service;

import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.client.REATClient;
import ua.com.epam.mode.geren.Genre;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import static ua.com.epam.config.URI.*;

@Log4j2
public class GenreService {
    private REATClient client;

    public GenreService(REATClient client) {
        this.client = client;
    }

    public Response getAllGenresByAuthorId(String authorId, String orderType, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("sortBy", sortBy);

        return getAllGenresByAuthorId(authorId, mapParam);
    }

    public Response getAllGenresByAuthorId(String authorId, Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting all genres by author id - {}.", authorId);
            response = client.get(String.format(GET_ALL_GENRE_BY_AUTHOR_ID, authorId, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get all genres by author id - {}..", authorId);
            throw e;
        }
        return response;
    }

    public Response getGenreByBookId(String bookId) {
        Response response = null;
        try {
            log.info("Start getting  genre by book id - {}.", bookId);
            response = client.get(String.format(GET_GENRE_BY_AUTHOR_ID, bookId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get genre by book id - {}..", bookId);
            throw e;
        }
        return response;
    }

    public Response createGenre(Genre genre) {
        Response response = null;
        try {
            log.info("Start creating genre - {}.", genre);
            response = client.post(CREATE_GENRE, genre);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create genre - {}.", genre);
            throw e;
        }
        return response;
    }

    public Response updateGenre(Genre genre) {
        Response response = null;
        try {
            log.info("Start updating author - {}.", genre);
            response = client.put(UPDATE_GENRE, genre);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't update Author - {}.", genre);
            throw e;
        }
        return response;
    }

    public Response getGenreById(String genreId) {
        Response response = null;
        try {
            log.info("Start getting genre by id - {}.", genreId);
            response = client.get(String.format(GET_GENRE_BY_ID, genreId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get genre by id - {}.", genreId);
            throw e;
        }
        return response;
    }

    public Response deleteGenre(String genreId) {
        Response response = null;
        try {
            log.info("Start deleting genre by id - {}.", genreId);
            response = client.delete(String.format(DELETE_GENRE_BY_ID, genreId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't delete genre by id - {}.", genreId);
            throw e;
        }
        return response;
    }

    public Response getAllGenre(String orderType, String number, String pagination, String size, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("page", number);
        mapParam.put("pagination", pagination);
        mapParam.put("size", size);
        mapParam.put("sortBy", sortBy);
        return getAllGenre(mapParam);
    }

    public Response getAllGenre(Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting all genres.");
            response = client.get(String.format(GET_ALL_GENRES, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get genres.");
            throw e;
        }
        return response;
    }

    public Response searchGenreByName(String genreName) {
        Response response = null;
        try {
            log.info("Start searching genre by name- {}.", genreName);
            response = client.get(String.format(SEARCH_GENRE_NAME, genreName));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't search genre by author by by name - {}.", genreName);
            throw e;
        }
        return response;
    }

    private String mapToParams(Map<String, String> mapParam) {
        StringJoiner sj = new StringJoiner("&");
        for (String key : mapParam.keySet()) {
            String value = mapParam.get(key);
            String param = key + "=" + encode(value);
            sj.add(param);
        }
        return "?" + sj.toString();
    }

    private static String encode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return value;
    }
}
