package ua.com.epam.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import static ua.com.epam.config.PropConstant.REST_ENDPOINT_URL;

public class DataProp {
    private Properties props = new Properties();

    public DataProp() {
        FileInputStream fis;

        try {
            fis = new FileInputStream("src/main/resources/data.properties");
            props.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String endpointUrl() {
        return props.getProperty(REST_ENDPOINT_URL);
    }
}