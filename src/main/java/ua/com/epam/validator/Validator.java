package ua.com.epam.validator;

import org.apache.commons.collections4.CollectionUtils;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import java.util.List;

import static ua.com.epam.config.AssertMessage.*;

public class Validator {
    private SoftAssert softAssert = new SoftAssert();

    public <T> void isNotListEmpty(List<T> response) {
        Assert.assertTrue(CollectionUtils.isNotEmpty(response), String.format(EMPTY_RESPONSE, response));
    }

    public <T> void checkSortId(List<Long> response, String sortBy) {

        if ("asc".equals(sortBy.toLowerCase())) {
            for (int i = 0; i < response.size() - 1; i++) {
                if (response.get(i) > response.get(i + 1)) {
                    Assert.fail(String.format(SORT_BY_MSG, sortBy));
                    break;
                }
            }
        } else if ("desc".equals(sortBy.toLowerCase())) {
            for (int i = 0; i < response.size() - 1; i++) {
                if (response.get(i) < response.get(i + 1)) {
                    Assert.fail(String.format(SORT_BY_MSG, sortBy));
                    break;
                }
            }
        } else {
            Assert.fail(String.format("Invalid parameter - '%s' for sorting.", sortBy));
        }
    }

    public <T> void checkResponseObject(T authorResp, T authorBuild) {
        Assert.assertEquals(authorResp, authorBuild, MSG_OBJECT);
    }

    public void checkStatusCode(int expectedCode, int statusCode) {
        Assert.assertEquals(expectedCode, statusCode, MSG_STATUS_CODE);
    }
}
