package ua.com.epam.mode;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Fault {
    private String timeStamp;
    private int statusCode;
    private String error;
    private String errorMessage;
}
