package ua.com.epam.config;

public interface AssertMessage {
    //Use String.format() for %s, %d.
    String MSG_STATUS_CODE = "Response status code is incorrect. Expected - %d but get - %d";
    String EMPTY_RESPONSE = "Empty response - %s.";
    String MSG_OBJECT = "Incorrect response object.";
    String SORT_BY_MSG = "Response isn't sorted by '%s'.";
    String ERROR_AUTHOR_MSG = "Author with 'authorId' = '%d' doesn't exist!";
    String ERROR_AUTHOR_MSG_CREATE = "Author with such 'authorId' already exists!";
    String GET_ALL_AUTHORS_ERROR_MSG_ORDER = "Order type must be 'asc' or 'desc' instead '%s'!";
}
