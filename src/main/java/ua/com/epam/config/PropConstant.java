package ua.com.epam.config;

public interface PropConstant {
    //API
    String REST_ENDPOINT_URL = "rest.endpoint.url";
    String CONTENT_TYPE = "application/json;charset=UTF-8";
}
