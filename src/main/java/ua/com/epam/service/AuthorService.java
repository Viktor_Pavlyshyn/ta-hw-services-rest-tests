package ua.com.epam.service;

import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.client.REATClient;
import ua.com.epam.mode.author.Author;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import static ua.com.epam.config.URI.*;

@Log4j2
public class AuthorService {
    private REATClient client;

    public AuthorService(REATClient client) {
        this.client = client;
    }

    public void AuthorService(REATClient client) {
        this.client = client;
    }

    public Response createAuthor(Author author) {
        Response response = null;
        try {
            log.info("Start creating author - {}.", author);
            response = client.post(POST_AUTHOR, author);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create Author - {}.", author);
            throw e;
        }
        return response;
    }

    public Response updateAuthor(Author author) {
        Response response = null;
        try {
            log.info("Start updating author - {}.", author);
            response = client.put(PUT_AUTHOR, author);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't update Author - {}.", author);
            throw e;
        }
        return response;
    }

    public Response getAuthorById(String authorId) {
        Response response = null;
        try {
            log.info("Start getting author by id - {}.", authorId);
            response = client.get(String.format(GET_AUTHOR_BY_ID, authorId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get author by id - {}.", authorId);
            throw e;
        }
        return response;
    }

    public Response deleteAuthor(String authorId) {
        Response response = null;
        try {
            log.info("Start deleting author by id - {}.", authorId);
            response = client.delete(String.format(DELETE_AUTHOR_BY_ID, authorId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't delete Author by id - {}.", authorId);
            throw e;
        }
        return response;
    }

    public Response getAllAuthors(String orderType, String number, String pagination, String size, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("page", number);
        mapParam.put("pagination", pagination);
        mapParam.put("size", size);
        mapParam.put("sortBy", sortBy);
        return getAllAuthors(mapParam);
    }

    public Response getAllAuthors(Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting all authors.");
            response = client.get(String.format(GET_ALL_AUTHORS, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get authors.");
            throw e;
        }
        return response;
    }

    public Response searchAuthorByNameOrSurname(String searchByNameOrSurname) {
        Response response = null;
        try {
            log.info("Start searching author by name or surname - {}.", searchByNameOrSurname);
            response = client.get(String.format(SEARCH_AUTHORS_BY_NAME_OR_SURNAME, searchByNameOrSurname));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't search author by author by by name or surname - {}.", searchByNameOrSurname);
            throw e;
        }
        return response;
    }

    public Response getAuthorByBookId(String bookId, String orderType, String number,
                                      String pagination, String size, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("page", number);
        mapParam.put("pagination", pagination);
        mapParam.put("size", size);
        mapParam.put("sortBy", sortBy);
        return getAuthorByBookId(bookId, mapParam);
    }

    public Response getAuthorByBookId(String id, Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting author by book id - {}.", id);
            response = client.get(String.format(GET_AUTHOR_BY_BOOK, id, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get author by book id - {}.", id);
            throw e;
        }
        return response;
    }

    public Response getAllAuthorsByGenreId(String genreId, String orderType, String number,
                                           String pagination, String size, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("page", number);
        mapParam.put("pagination", pagination);
        mapParam.put("size", size);
        mapParam.put("sortBy", sortBy);
        return getAllAuthorsByGenreId(genreId, mapParam);
    }

    public Response getAllAuthorsByGenreId(String id, Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting author by genre id - {}.", id);
            response = client.get(String.format(GET_ALL_AUTHORS_BY_GENRE, id, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get author by genre id - {}.", id);
            throw e;
        }
        return response;
    }

    private String mapToParams(Map<String, String> mapParam) {
        StringJoiner sj = new StringJoiner("&");
        for (String key : mapParam.keySet()) {
            String value = mapParam.get(key);
            String param = key + "=" + encode(value);
            sj.add(param);
        }
        return "?" + sj.toString();
    }

    private static String encode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return value;
    }
}