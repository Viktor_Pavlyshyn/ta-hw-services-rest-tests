package ua.com.epam.mode.geren;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Genre {
    private Long genreId;
    private String genreName;
    private String genreDescription;
}
