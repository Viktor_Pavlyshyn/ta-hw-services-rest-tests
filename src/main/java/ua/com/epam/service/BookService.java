package ua.com.epam.service;

import io.restassured.response.Response;
import lombok.extern.log4j.Log4j2;
import ua.com.epam.client.REATClient;
import ua.com.epam.mode.book.Book;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;

import static ua.com.epam.config.URI.*;

@Log4j2
public class BookService {
    private REATClient client;

    public BookService(REATClient client) {
        this.client = client;
    }

    public Response getAllBooksByAuthorId(String authorId, String orderType, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("sortBy", sortBy);

        return getAllBooksByAuthorId(authorId, mapParam);
    }

    public Response getAllBooksByAuthorId(String id, Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting book by author id - {}.", id);
            response = client.get(String.format(GET_ALL_BOOKS_BY_AUTHOR_ID, id, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by author id - {}.", id);
            throw e;
        }
        return response;
    }

    public Response getAllBooksByAuthorIdAndGenreId(String authorId, String genreId) {
        Response response = null;
        try {
            log.info("Start getting book by author id - {} and genre id - {}.", authorId, genreId);
            response = client.get(String.format(GET_ALL_BOOKS_BY_AUTHOR_AND_GENRE_ID, authorId, genreId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by author id - {} and genre id - {}.", authorId, genreId);
            throw e;
        }
        return response;
    }

    public Response updateBook(Book book) {
        Response response = null;
        try {
            log.info("Start updating book - {}.", book);
            response = client.put(UPDATE_BOOKS, book);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't update book - {}.", book);
            throw e;
        }
        return response;
    }

    public Response createBook(String authorId, String genreId, Book book) {
        Response response = null;
        try {
            log.info("Start creating book - {}.", book);
            response = client.post(String.format(CREATE_BOOKS, authorId, genreId),  book);
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create book - {}.", book);
            throw e;
        }
        return  response;
    }

    public Response getBookById(String bookId) {
        Response response = null;
        try {
            log.info("Start getting book id - {}.", bookId);
            response = client.get(String.format(GET_BOOK_BY_ID, bookId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by id - {}.", bookId);
            throw e;
        }
        return response;
    }

    public Response deleteBook(String bookId) {
        Response response = null;
        try {
            log.info("Start deleting book by id - {}.", bookId);
            response = client.delete(String.format(DELETE_BOOK_BY_ID, bookId));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't create book by id - {}.", bookId);
            throw e;
        }
        return response;
    }

    public Response getAllBooks(String orderType, String number, String pagination, String size, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("page", number);
        mapParam.put("pagination", pagination);
        mapParam.put("size", size);
        mapParam.put("sortBy", sortBy);
        return getAllBooks(mapParam);

    }

    public Response getAllBooks(Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting all books.");
            response = client.get(String.format(GET_ALL_BOOKS, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get all books.");
            throw e;
        }
        return response;
    }

    public Response searchBookByBookName(String bookName) {
        Response response = null;
        try {
            log.info("Start searching book by name - {}.", bookName);
            response = client.get(String.format(SEARCH_BOOK_BY_NAME, bookName));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't search book by name - {}.", bookName);
            throw e;
        }
        return response;
    }

    public Response getAllBooksByGenreId(String genreId, String orderType, String number, String pagination, String size, String sortBy) {
        Map<String, String> mapParam = new HashMap<>();
        mapParam.put("orderType", orderType);
        mapParam.put("page", number);
        mapParam.put("pagination", pagination);
        mapParam.put("size", size);
        mapParam.put("sortBy", sortBy);
        return getAllBooksByGenreId(genreId, mapParam);
    }

    public Response getAllBooksByGenreId(String genreId, Map<String, String> mapParam) {
        Response response = null;
        try {
            log.info("Start getting book by genre id - {}.", genreId);
            response = client.get(String.format(SEARCH_BOOK_BY_GENRE_ID, genreId, mapToParams(mapParam)));
            log.info("Successful return response.");
        } catch (Exception e) {
            log.error("Can't get book by genre id - {}.", genreId);
            throw e;
        }
        return response;
    }

    private String mapToParams(Map<String, String> mapParam) {
        StringJoiner sj = new StringJoiner("&");
        for (String key : mapParam.keySet()) {
            String value = mapParam.get(key);
            String param = key + "=" + encode(value);
            sj.add(param);
        }
        return "?" + sj.toString();
    }

    private static String encode(String value) {
        try {
            return URLEncoder.encode(value, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return value;
    }
}
