package ua.com.epam.mode.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Additional {
    private Long pageCount;
    private Size size;
}
