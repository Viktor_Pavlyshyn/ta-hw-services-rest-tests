package ua.com.epam;

import io.restassured.response.Response;
import org.testng.annotations.Test;
import ua.com.epam.mode.Fault;
import ua.com.epam.mode.author.Author;

import java.util.List;
import java.util.stream.Collectors;

import static ua.com.epam.config.AssertMessage.GET_ALL_AUTHORS_ERROR_MSG_ORDER;

public class GetAllAuthorTest extends BaseTest {

    @Test
    public void getAllAuthorsIsEmpty() {

        Response response = authorService.
                getAllAuthors("asc", "1", "true", "10", "authorId");

        int statusCode = response.getStatusCode();
        validator.checkStatusCode(200, statusCode);

        List<Author> lisAuthor = response.jsonPath().getList("$", Author.class);
        validator.isNotListEmpty(lisAuthor);
    }

    @Test
    public void getAllAuthorsByFalseParam() {

        Response response = authorService.
                getAllAuthors("test", "1", "true", "10", "authorId");

        int statusCode = response.getStatusCode();
        validator.checkStatusCode(400, statusCode);

        Fault fault = response.jsonPath().getObject("$", Fault.class);
        String errorMsg = fault.getErrorMessage();

        validator.checkResponseObject(errorMsg, String.format(GET_ALL_AUTHORS_ERROR_MSG_ORDER, "test"));
    }

    @Test
    public void getAllAuthorsCheckSort() {

        Response response = authorService.
                getAllAuthors("asc", "1", "true", "10", "authorId");

        int statusCode = response.getStatusCode();
        validator.checkStatusCode(200, statusCode);

        List<Author> lisAuthor = response.jsonPath().getList("$", Author.class);
        List<Long> authorIdList = lisAuthor.stream().map(Author::getAuthorId).collect(Collectors.toList());

        validator.checkSortId(authorIdList, "asc");
    }
}
